# 5CTO-DM-INDEX_GODEV



## Getting started

```
git clone https://gitlab.com/thomas.tilly/5cto-dm-index_godev.git

go build

go run main.go
```

## Rules

✅ Tri: 1. Versions DESC, 2. Modules DESC, 3. Forge ASC  
✅ Spécification du protocole sur https://index.golang.org/.  
✅ Inclure le header Disable-Module-Fetch: true pour limiter la charge soumise à Google  
✅ Inclure le traitement des erreurs.  
✅ Désérialisation JSON par encoding/json, table par tabwriter.  
✅ Valider le code avec "golint -min_confidence=0.3 ." et "golangci-lint run"  
Bonus: expliquer la raison pour les chiffres que vous allez obtenir concernant k8s.
