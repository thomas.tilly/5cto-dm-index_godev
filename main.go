package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"text/tabwriter"
)

// ModuleData stocke les informations sur chaque module
type ModuleData struct {
	Path      string `json:"Path"`
	Version   string `json:"Version"`
	Timestamp string `json:"Timestamp"`
}

// ForgeStats stocke les statistiques pour chaque forge
type ForgeStats struct {
	Name     string
	Modules  int
	Versions int
}

// FetchModules interroge l'API et retourne un slice de ModuleData
func FetchModules(apiURL string) ([]ModuleData, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Disable-Module-Fetch", "true")
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var modules []ModuleData
	dec := json.NewDecoder(strings.NewReader(string(body)))
	for {
		var module ModuleData
		if err := dec.Decode(&module); err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
		modules = append(modules, module)
	}

	return modules, nil
}

// AggregateStats regroupe les données par forge
func AggregateStats(modules []ModuleData) []ForgeStats {
	statsMap := make(map[string]*ForgeStats)
	moduleCount := make(map[string]map[string]bool) // Map pour suivre les modules uniques par forge

	for _, m := range modules {
		forgeName := strings.Split(m.Path, "/")[0]
		if _, exists := statsMap[forgeName]; !exists {
			statsMap[forgeName] = &ForgeStats{Name: forgeName}
			moduleCount[forgeName] = make(map[string]bool)
		}

		statsMap[forgeName].Versions++
		if _, moduleExists := moduleCount[forgeName][m.Path]; !moduleExists {
			statsMap[forgeName].Modules++
			moduleCount[forgeName][m.Path] = true
		}
	}

	var stats []ForgeStats
	for _, stat := range statsMap {
		stats = append(stats, *stat)
	}

	return stats
}

func main() {
	apiURL := "https://index.golang.org/index"
	modules, err := FetchModules(apiURL)
	if err != nil {
		log.Fatalf("Error fetching modules: %v", err)
	}

	stats := AggregateStats(modules)

	// Calcul des totaux
	totalModules, totalVersions := 0, 0
	for _, stat := range stats {
		totalModules += stat.Modules
		totalVersions += stat.Versions
	}

	// Création du writer
	writer := tabwriter.NewWriter(os.Stdout, 0, 8, 1, '\t', 0)

	// Fonction pour afficher les statistiques avec surlignage
	displayStats := func(title string, sortingFunc func(i, j int) bool, colorCode string) {
		fmt.Fprintf(writer, "\033[%sm%s\033[0m\n", colorCode, title)
		fmt.Fprintf(writer, "\033[1;34m%-30s %-10s %-10s\033[0m\n", "Forge", "Modules", "Versions")
		sort.Slice(stats, sortingFunc)
		for _, stat := range stats {
			fmt.Fprintf(writer, "%-30s %-10d %-10d\n", stat.Name, stat.Modules, stat.Versions)
		}
		fmt.Fprintf(writer, "%-30s %-10d %-10d\n", "_Totals_", totalModules, totalVersions)
		writer.Flush()
		fmt.Fprintln(writer)
	}

	// Affichage des statistiques avec les trois tris
	displayStats("TRI 1 : Versions DESC", func(i, j int) bool {
		return stats[i].Versions > stats[j].Versions
	}, "31")

	displayStats("TRI 2 : Modules DESC", func(i, j int) bool {
		return stats[i].Modules > stats[j].Modules
	}, "32")

	displayStats("TRI 3 : Forge ASC", func(i, j int) bool {
		return stats[i].Name < stats[j].Name
	}, "33")
}
